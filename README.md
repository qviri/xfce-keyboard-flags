This is for a nicer font for xfce4-xkb-plugin so that the keyboard layout indicator is not super prominent.
The information is from http://forum.xfce.org/viewtopic.php?id=8200 and the icons are largely work from there.

To set up, copy the files into /usr/share/xfce4/xkb/flags then configure xfce4-xkb-plugin to show layout as image.

Modify the files for other languages as needed. Inkscape works, though you might be able to just edit the SVG source (it's XML) to change the string and its x/y position.

